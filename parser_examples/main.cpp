
int main()
{
    extern void parser_xml_example();
    parser_xml_example();
    
    extern void parser_hello_world_example();
    parser_hello_world_example();
        
    extern void parser_json_example();
    parser_json_example();
    
    extern void parser_calculator_example();
    parser_calculator_example();
    
    extern void parser_hello_world_with_grammar_example();
    parser_hello_world_with_grammar_example();
    
    extern void parser_calculator_with_grammar_example();
    parser_calculator_with_grammar_example();

    return 0;
}
