//
// parser_types.cpp
// Copyright (c) 2009 - 2011 Charles Baker.  All rights reserved.
//

#include "stdafx.hpp"
#include "parser_types.hpp"

/** 
// The keyword used to represent the special 'error' terminal. 
*/
SWEET_PARSER_DECLSPEC const char* sweet::parser::ERROR_KEYWORD = "error";
