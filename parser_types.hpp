//
// parser_types.hpp
// Copyright (c) 2009 - 2011 Charles Baker.  All rights reserved.
//

#ifndef PARSER_TYPES_HPP_INCLUDED
#define PARSER_TYPES_HPP_INCLUDED

#include "declspec.hpp"

namespace sweet
{

namespace parser
{

SWEET_PARSER_DECLSPEC extern const char* ERROR_KEYWORD;

}

}

#endif